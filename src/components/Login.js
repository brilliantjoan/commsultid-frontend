import React, { useState, useContext } from 'react'
import { Link, useHistory } from 'react-router-dom'
import localforage from 'localforage'
import _ from 'lodash'

import GetApi from './api/GetApi'
import { LoginContext } from './context/LoginContext'

import TwitterLogo from '../static/image/twitter-logo.png'

function Login () {
  const [login, setLogin] = useContext(LoginContext)
  const [values, setValues] = useState({
    email: '',
    password: ''
  })
  const [errorMessage, setErrorMessage] = useState('')
  const history = useHistory()

  const loginUser = async () => {
    if (values.email && values.password) {
      try {
        const response = await GetApi.loginUser(values)
        const res = await response.data
        if (res.message === 'Success') {
          setLogin(res.data)
          await localforage.setItem('token', res.data)
          history.push('/home')
        } else {
          setErrorMessage(res.data)
        }
      } catch (error) {
        console.log(error)
      }
    }
  }

  const handleEmailChange = (e) => {
    setValues((val) => ({
      ...val,
      email: e.target.value
    }))
  }

  const handlePasswordChange = (e) => {
    setValues((val) => ({
      ...val,
      password: e.target.value
    }))
  }

  return (
    <div className='container'>
      <div className='row'>
        <div className='col-xl-5 col-lg-6 col-md-8 col-sm-10 mx-auto mt-5 text-center form mt-5 pt-1 pl-4 pr-4 pb-4 bg-white'>
          <div className='row'>
            <div className='col-2'>
              <img src={TwitterLogo} alt='' className='w-100' />
            </div>
          </div>
          <div className='row mt-3 ml-2'>
            <div className='h1 font-weight-bold'>Log in to Twitter</div>
          </div>
          <div className='row mt-2 ml-2'>
            <div className='form-group w-75'>
              <input type='email' className='form-control shadow-none' placeholder='Phone, email, or username' value={values.email} onChange={handleEmailChange} />
            </div>
          </div>
          <div className='row mt-2 ml-2'>
            <div className='form-group w-75'>
              <input type='password' className='form-control shadow-none' placeholder='Password' value={values.password} onChange={handlePasswordChange} />
            </div>
          </div>
          <div className='row'>
            <div className='form-group ml-4 text-danger'>
              {!_.isEmpty(errorMessage)
                ? <div>{errorMessage}</div>
                : null}
            </div>
          </div>
          <div className='row ml-2 w-75'>
            {!_.isEmpty(values.email) && !_.isEmpty(values.password)
              ? <div className='btn btn-primary rounded-pill w-100 p-2 font-weight-bold' onClick={loginUser}>
                Login
              </div>
              : <div className='btn btn-primary rounded-pill w-100 p-2 font-weight-bold disabled' disabled>
                Login
              </div>}
          </div>
          <div className='row mt-4 mr-5 justify-content-center'>
            <div className='text-primary' style={{ cursor: 'pointer' }}>
              Forgot password?
            </div>
            <div className='text-primary'>
              ·
            </div>
            <Link to='/signup' className='text-primary'>
              Sign up for Twitter
            </Link>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Login
