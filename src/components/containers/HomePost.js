import React, { useContext, useEffect, useState, useRef } from 'react'
import { useHistory } from 'react-router-dom'
import _ from 'lodash'
import moment from 'moment'

import GetApi from '../api/GetApi'
import { PostContext } from '../context/PostContext'
import { UserContext } from '../context/UserContext'
import { SearchContext } from '../context/SearchContext'

import twitterAvatar from '../../static/image/twitterAvatar.png'
import Image from '../../static/image/image.png'
import Gif from '../../static/image/gif.png'
import Poll from '../../static/image/poll.png'
import Smiley from '../../static/image/smiley.png'
import Calendar from '../../static/image/calendar.png'
import Reply from '../../static/image/reply.png'
import Retweet from '../../static/image/retweet.png'
import Like from '../../static/image/like.png'
import Trash from '../../static/image/trash.png'
import More from '../../static/image/more.png'

function HomePost () {
  const [post, setPost] = useContext(PostContext)
  const [user, setUser] = useContext(UserContext)
  const [search, setSearch] = useContext(SearchContext)
  const [sorted, setSorted] = useState()
  const [values, setValues] = useState({
    text: '',
    imagePreview: '',
    image: ''
  })
  const [edit, setEdit] = useState({
    id: '',
    isEdit: false
  })
  const [editData, setEditData] = useState({
    id: '',
    user_id: '',
    text: '',
    image: ''
  })
  const inputFile = useRef(null)
  const history = useHistory()

  useEffect(() => {
    getAllPosts()
  }, [])

  const getAllPosts = async () => {
    try {
      const response = await GetApi.getAllPosts('DESC')
      const res = await response.data.data
      setPost(res)
    } catch (error) {
      console.log(error)
    }
  }

  const handleTextChange = (e) => {
    setValues((val) => ({
      ...val,
      text: e.target.value
    }))
  }

  const onImageClick = () => {
    inputFile.current.click()
  }

  const handleImageChange = (e) => {
    if (e.target.files && e.target.files[0]) {
      console.log('val', e.target.files[0])
      const files = new FormData()
      files.append('file', e.target.files[0])
      setValues((val) => ({
        ...val,
        imagePreview: URL.createObjectURL(e.target.files[0]),
        image: files
      }))
    }
  }

  const editClick = (data) => {
    console.log('data', data)
    setEdit((val) => ({
      ...val,
      id: data.id,
      isEdit: true
    }))
    setEditData((val) => ({
      ...val,
      user_id: data.id_user,
      id: data.id,
      text: data.text,
      image: data.image
    }))
  }

  const handleEditDataChange = (e) => {
    setEditData((val) => ({
      ...val,
      text: e.target.value
    }))
  }

  const insertNewPost = async () => {
    const body = {
      user_id: user.id,
      image: values.image,
      text: values.text,
      reply_id: null
    }
    console.log('body', body)
    const response = await GetApi.insertNewPost(body)
    const res = await response.data
    if (res.message === 'Success') {
      history.go(0)
    }
  }

  const updateData = async () => {
    const body = {
      id: editData.id,
      text: editData.text,
      image: editData.image
    }
    const response = await GetApi.updatePost(body)
    const res = await response.data
    if (res.message === 'Success') {
      history.go(0)
    }
  }

  const deleteData = async (id) => {
    const body = {
      id: id
    }
    const response = await GetApi.deletePost(body)
    const res = await response.data
    if (res.message === 'Success') {
      history.go(0)
    }
  }

  // const setGetData = async (filter) => {
  //   const response = await GetApi.getAllPosts(filter)
  //   const res = await response.data.data
  //   setPost(res)
  // }

  const sortData = (sort) => {
    if (sort === 'asc') {
      setPost([...post].sort((a, b) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0)))
    } else {
      setPost([...post].sort((a, b) => (a.id < b.id) ? 1 : ((b.id < a.id) ? -1 : 0)))
    }
  }

  const bodyData = (data) => {
    const now = moment(new Date())
    const end = moment(data.created_at)
    const duration = moment.duration(now.diff(end)).abs()
    let dur = parseInt(duration.asHours())
    let key = 'h'
    if (dur === 0) {
      dur = parseInt(duration.asMinutes())
      key = 'm'
    }

    return (
      <div className='row pt-3 pb-2 border-bottom' key={data.id}>
        <div className='col-2'>
          <div>
            <img src={twitterAvatar} alt='' className='border rounded-circle w-75' style={{ cursor: 'pointer' }} />
          </div>
        </div>
        <div className='col-8 pl-0 pr-0'>
          <div className='row' style={{ fontSize: '15px' }}>
            <span className='font-weight-bold'>
              {data.name}
            </span>
            <span className='ml-1 text-muted'>
              @{data.username}
            </span>
            <span className='ml-2 text-muted'>
              ·
            </span>
            <span className='ml-2 text-muted'>
              {dur}{key}
            </span>
          </div>
          {edit.isEdit === true && edit.id === data.id
            ? <div className='row' style={{ fontSize: '15px' }}>
              <input type='textarea' placeholder="What's Happening?" className='form-control-plaintext shadow-none' style={{ outline: 'none', fontSize: '14px' }} onChange={handleEditDataChange} value={editData.text} maxLength='240' />
              </div>
            : <div className='row' style={{ fontSize: '15px' }}>
              {data.text}
              </div>}
          {_.isEmpty(data.image)
            ? null
            : <div className='row mt-2'>
              <img src={require('../../static/upload/' + data.image).default} alt='' className='w-100 border' style={{ borderRadius: '15px' }} />
              </div>}
          <div className='row mt-3'>
            <div className='col-3 pr-0'>
              <div className='row'>
                <div className='col-4 pr-0 my-auto'>
                  <img src={Reply} alt='' className='w-75' style={{ cursor: 'pointer' }} />
                </div>
                <div className='col-2'>
                  <div className=''>
                    {data.reply}
                  </div>
                </div>
              </div>
            </div>
            <div className='col-3 pr-0'>
              <div className='row'>
                <div className='col-4 pr-0'>
                  <img src={Retweet} alt='' className='w-100' style={{ cursor: 'pointer' }} />
                </div>
                <div className='col-2 my-auto'>
                  <div className=''>
                    {data.retweet}
                  </div>
                </div>
              </div>
            </div>
            <div className='col-3 pr-0'>
              <div className='row'>
                <div className='col-4 pr-0'>
                  <img src={Like} alt='' className='w-100' style={{ cursor: 'pointer' }} />
                </div>
                <div className='col-2 my-auto'>
                  <div className=''>
                    {data.likes}
                  </div>
                </div>
              </div>
            </div>
            <div className='col-3 pr-0'>
              <div className='row'>
                <div className='col-4 pr-0' onClick={() => deleteData(data.id)}>
                  <img src={Trash} alt='' className='w-75' style={{ cursor: 'pointer' }} />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-2'>
          <div className='row'>
            {edit.isEdit === true && edit.id === data.id
              ? <div className='btn btn-outline-primary rounded-pill font-weight-bold' style={{ fontSize: '12px' }} onClick={updateData}> Update </div>
              : <img src={More} alt='' className='ml-5 w-25' onClick={() => editClick(data)} style={{ cursor: 'pointer' }} />}
          </div>
        </div>
      </div>
    )
  }

  return (
    <div className='col-6 ml-5 border-left border-right'>
      <div className='row border-bottom sticky-top bg-white'>
        <div className='p-3'>
          <span className='font-weight-bold h5'>Home</span>
        </div>
      </div>
      <div className='row'>
        <div className='ml-3 mr-3'>
          <div className='mt-3'>
            <div className='row'>
              <div className='col-2'>
                <img src={twitterAvatar} alt='' className='border rounded-circle w-75' style={{ cursor: 'pointer' }} />
              </div>
              <div className='col-10 my-auto'>
                <div className='row'>
                  <input type='textarea' placeholder="What's Happening?" className='form-control-plaintext shadow-none' style={{ outline: 'none', fontSize: '20px' }} onChange={handleTextChange} value={values.text} maxLength='250' />
                </div>
              </div>
              <div className='container mt-3'>
                <div className='row justify-content-end pb-2'>
                  <div className='col-1 my-auto pr-0'>
                    <div className='row'>
                      <img src={Image} alt='' className='w-50' onClick={onImageClick} style={{ cursor: 'pointer' }} />
                      <input type='file' className='d-none' ref={inputFile} accept='image/png, image/jpg, image/jpeg' onChange={handleImageChange} />
                      {!_.isEmpty(values.image)
                        ? <img src={values.image} alt='' className='w-100' />
                        : null}
                    </div>
                  </div>
                  <div className='col-1 my-auto pr-0'>
                    <div className='row'>
                      <img src={Gif} alt='' className='w-50' style={{ cursor: 'pointer' }} />
                    </div>
                  </div>
                  <div className='col-1 my-auto pr-0'>
                    <div className='row'>
                      <img src={Poll} alt='' className='w-50' style={{ cursor: 'pointer' }} />
                    </div>
                  </div>
                  <div className='col-1 my-auto pr-0'>
                    <div className='row'>
                      <img src={Smiley} alt='' className='w-50' style={{ cursor: 'pointer' }} />
                    </div>
                  </div>
                  <div className='col-1 my-auto pr-0'>
                    <div className='row'>
                      <img src={Calendar} alt='' className='w-50' style={{ cursor: 'pointer' }} />
                    </div>
                  </div>
                  <div className='col-3' />
                  <div className='col-2 my-auto mr-3'>
                    <div className='ml-2'>
                      <div className='row float-right'>
                        {values.text.length >= 1 && values.text.length <= 240
                          ? <div className='btn btn-primary rounded-pill font-weight-bold border-0' style={{ fontSize: '14px' }} onClick={insertNewPost}>Tweet</div>
                          : <div className='btn btn-primary rounded-pill font-weight-bold border-0 disabled' style={{ fontSize: '14px' }}>Tweet</div>}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='container'>
          <div className='row bg-light border-top border-bottom'>
            <hr className='h-50 bg-secondary mt-25 mb-0' />
          </div>
        </div>
        <div className='container border-bottom'>
          <div className='py-2 mx-2'>
            <div className='btn btn-primary rounded-pill' onClick={() => sortData('asc')}>Ascending</div>
            <div className='btn btn-primary rounded-pill' onClick={() => sortData('desc')}>Descending</div>
          </div>
        </div>
        <div className='container'>
          {!_.isEmpty(post)
            ? search
                ? post.filter(data => data.text.indexOf(search) > -1).map((data) => {
                    return bodyData(data)
                  })
                : post.map((data) => {
                  console.log('data', data)
                  return bodyData(data)
                })
            : null}
        </div>
      </div>
    </div>
  )
}

export default HomePost
